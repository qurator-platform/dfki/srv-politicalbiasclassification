#!/usr/bin/python3
import json
import os

from flask import Flask, Response, flash, redirect, request, url_for
from flask_cors import CORS
from werkzeug.utils import secure_filename

# custom libs
from BiasPredictor import biasPredictor

"""
to start run:
export FLASK_APP=flaskController.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:

curl -X GET localhost:5000/detectLanguages?input="aap"

curl -F 'pdffile=@temp_pdf_storage/machine_readable_single_column_2.pdf' -X POST localhost:5000/ocr

curl -X POST localhost:5000/ -H 'Content-Type: application/json' -H 'Accept: text/event-stream, application/json' -d '{"mimeType": "text/plain", "content": "This is a test from Berlin."}'

"""


app = Flask(__name__)
app.secret_key = "super secret key"
CORS(app)

SUPPORTED_NIFFORMATS = ['turtle', 'xml', 'json-ld']


@app.route('/welcome', methods=['GET'])
def dummy():
    return "Hello stranger, can you tell us where you've been?\nMore importantly, how ever did you come to be here?\n"




##################### entity spotting #####################
@app.route('/getBias', methods=['GET', 'POST'])
def getBias():

    """
    Required args:
    - informat (turtle, txt, etc.)
    - outformat (turtle, other nif-usuals)
    - input (actual input text to spot entities in)
    """
    
    supported_informats = ['txt']
    #supported_informats.extend(SUPPORTED_NIFFORMATS)
    #supported_outformats = SUPPORTED_NIFFORMATS
    inp = None
    if request.args.get('input') == None:
        if request.data == None:
            return 'Please provide some text as input.\n'
        else:
            inp = request.data.decode('utf-8')
    else:
        inp = request.args.get('input')
    # only allowing plaintext in for now
    #if request.args.get('informat') == None:
        #return 'Please specify input format (currently supported: %s)\n' % str(supported_informats)
    #elif request.args.get('informat') not in supported_informats:
        #return 'Input format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('informat'), (str(supported_informats)))
    # only outputting the label right now (TODO: make NIF annotation?)
    #if request.args.get('outformat') == None:
        #return 'Please specify output format (currently supported: %s)\n' % str(supported_outformats)
    #elif request.args.get('outformat') not in supported_outformats:
        #return 'Output format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('outformat'), (str(supported_outformats)))

    informat = request.args.get('informat')
    #outformat = request.args.get('outformat')
    

    # loading model in method (i.e., every time), bit inefficient for now, but for later, when we (hopefully, at some point) have multiple models (for ex. for DE, EN, etc.)
    predictor = biasPredictor('models/de_crawled') # make into arg when more are available

    label = predictor.predict(text = inp)
        
    return label    
    

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8080))
    app.run(host='localhost', port=port, debug=True)
